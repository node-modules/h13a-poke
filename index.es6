module.exports = poke;

function poke(object, instructions){

  let isNumeric = function(n) { return !isNaN(parseFloat(n)) && isFinite(n); }
  let isObject  = function(n) { return (typeof n === 'object'); }
  let isArray   = function(n) { return (Object.prototype.toString.call(n) === '[object Array]'); }
  let canEnter  = function(n) { return ((!!n)&&(isObject(n)||isArray(n))) }

  let dataEntry = function(object, part, type) {

    if(!object) object = {};

    if( canEnter( object[part.name]) ){

      return object[part.name];

    }else if( part.isLast && object[part.name]){

      // cannot enter this object, it is the last in path
      return object[part.name];

    }else{

      if(part.isLast){
        // this is the last node, the last node should be set to null
        object[part.name] = null;

      } else if( part.isNextNodeNumeric ){
        // if next one is numeric, then this one must be array
        object[part.name] = [];
        return object[part.name];

      }else{
        // default state in force entry is to create a property that is an object.
        object[part.name] = {};
        return object[part.name];
      }

    }

  }

  instructions.forEach((instruction)=>{

    let parts = instruction.path.split('.');

    parts = parts.map((name,index)=>{
      let isFirst = false;
      let isLast = false;
      let counter = index+1;
      if(counter == 1) isFirst = true;
      if(counter == parts.length) isLast = true;
      let nextNode = null;
      let prevNode = null;
      if(!isFirst){
        prevNode=parts[index-1]
      }
      if(!isLast){
        nextNode=parts[index+1]
      }
      let isPrevNodeNumeric = isNumeric(prevNode);
      let isThisNodeNumeric = isNumeric(name);
      let isNextNodeNumeric = isNumeric(nextNode);

      let response = {name,index,isFirst,isLast, nextNode,prevNode, isPrevNodeNumeric, isThisNodeNumeric, isNextNodeNumeric };
      return response;
    });

    //console.dir(parts)




    let currentWorkinLocation = object;
    let localPath = [];

    parts.forEach((part, partIndex)=>{

      // log the path for debug
      localPath.push(part);
      //console.log(`location ${localPath.map(i=>i.name).join("/")}`)

      let parent = currentWorkinLocation;
      let entered = dataEntry(currentWorkinLocation, part);


      if(part.isLast){

        //currentWorkinLocation = instruction.value;
        if(parent[part.name] != instruction.value){
          //console.log(`CHANGING ${part.name} in location ${localPath.map(i=>i.name).join("/")} from ${parent[part.name]} to ${instruction.value}`)
          parent[part.name] = instruction.value;

        }
        // console.log(`PARENT LOCATION ${localPath.map(i=>i.name).join("/")}`, parent);
        // console.log(`FINAL LOCATION ${localPath.map(i=>i.name).join("/")}`, parent[part.name]);

      }

      currentWorkinLocation = entered;
    });


  });

  //console.log(object)
  return object;
}
